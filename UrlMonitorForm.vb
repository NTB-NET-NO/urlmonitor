Imports System.Configuration
Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Net.Mail
Imports System.Net
Imports System.Xml


Public Class UrlMonitorForm
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        If ConfigurationManager.AppSettings("startupMinimized") = "true" Then
            minimized = True
            Me.WindowState = FormWindowState.Minimized
            Me.Hide()
        End If

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CheckTimer As System.Windows.Forms.Timer
    Friend WithEvents TrayIcon As System.Windows.Forms.NotifyIcon
    Friend WithEvents URL_List As System.Windows.Forms.ComboBox
    Friend WithEvents URL As System.Windows.Forms.TextBox
    Friend WithEvents Changed As System.Windows.Forms.TextBox
    Friend WithEvents Checked As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Content As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents PageSize As System.Windows.Forms.TextBox
    Friend WithEvents TrayMenu As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents TrayMenuOpen As System.Windows.Forms.MenuItem
    Friend WithEvents TrayMenuExit As System.Windows.Forms.MenuItem
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents rxContent As System.Windows.Forms.TextBox
    Friend WithEvents OpenButton As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UrlMonitorForm))
        Me.CheckTimer = New System.Windows.Forms.Timer(Me.components)
        Me.TrayIcon = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.TrayMenu = New System.Windows.Forms.ContextMenu
        Me.TrayMenuOpen = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.TrayMenuExit = New System.Windows.Forms.MenuItem
        Me.URL_List = New System.Windows.Forms.ComboBox
        Me.URL = New System.Windows.Forms.TextBox
        Me.PageSize = New System.Windows.Forms.TextBox
        Me.Changed = New System.Windows.Forms.TextBox
        Me.Checked = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Content = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.OpenButton = New System.Windows.Forms.Button
        Me.Label7 = New System.Windows.Forms.Label
        Me.rxContent = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'CheckTimer
        '
        Me.CheckTimer.Interval = 1000
        '
        'TrayIcon
        '
        Me.TrayIcon.ContextMenu = Me.TrayMenu
        Me.TrayIcon.Icon = CType(resources.GetObject("TrayIcon.Icon"), System.Drawing.Icon)
        Me.TrayIcon.Text = "NTB UrlMonitor"
        Me.TrayIcon.Visible = True
        '
        'TrayMenu
        '
        Me.TrayMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.TrayMenuOpen, Me.MenuItem2, Me.TrayMenuExit})
        '
        'TrayMenuOpen
        '
        Me.TrayMenuOpen.DefaultItem = True
        Me.TrayMenuOpen.Index = 0
        Me.TrayMenuOpen.Text = "&Open"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "-"
        '
        'TrayMenuExit
        '
        Me.TrayMenuExit.Index = 2
        Me.TrayMenuExit.Text = "E&xit"
        '
        'URL_List
        '
        Me.URL_List.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.URL_List.Location = New System.Drawing.Point(10, 37)
        Me.URL_List.Name = "URL_List"
        Me.URL_List.Size = New System.Drawing.Size(691, 24)
        Me.URL_List.TabIndex = 0
        '
        'URL
        '
        Me.URL.Location = New System.Drawing.Point(144, 83)
        Me.URL.Name = "URL"
        Me.URL.ReadOnly = True
        Me.URL.Size = New System.Drawing.Size(557, 22)
        Me.URL.TabIndex = 1
        '
        'PageSize
        '
        Me.PageSize.Location = New System.Drawing.Point(144, 138)
        Me.PageSize.Name = "PageSize"
        Me.PageSize.ReadOnly = True
        Me.PageSize.Size = New System.Drawing.Size(557, 22)
        Me.PageSize.TabIndex = 2
        '
        'Changed
        '
        Me.Changed.Location = New System.Drawing.Point(144, 166)
        Me.Changed.Name = "Changed"
        Me.Changed.ReadOnly = True
        Me.Changed.Size = New System.Drawing.Size(557, 22)
        Me.Changed.TabIndex = 3
        '
        'Checked
        '
        Me.Checked.Location = New System.Drawing.Point(144, 111)
        Me.Checked.Name = "Checked"
        Me.Checked.ReadOnly = True
        Me.Checked.Size = New System.Drawing.Size(557, 22)
        Me.Checked.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(10, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(556, 19)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Velg hvilken URL du �nsker informasjon om (Alle URL'er overv�kes kontinuerlig) :"
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(10, 65)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(691, 9)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(10, 83)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 23)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "URL"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(10, 111)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(120, 23)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Sist sjekket"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(10, 166)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 23)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Endringsdato"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(10, 138)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(120, 24)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "St�rrelse"
        '
        'Content
        '
        Me.Content.Location = New System.Drawing.Point(144, 234)
        Me.Content.Multiline = True
        Me.Content.Name = "Content"
        Me.Content.ReadOnly = True
        Me.Content.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.Content.Size = New System.Drawing.Size(557, 212)
        Me.Content.TabIndex = 12
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(10, 234)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(120, 23)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Innhold"
        '
        'OpenButton
        '
        Me.OpenButton.Location = New System.Drawing.Point(10, 409)
        Me.OpenButton.Name = "OpenButton"
        Me.OpenButton.Size = New System.Drawing.Size(124, 27)
        Me.OpenButton.TabIndex = 14
        Me.OpenButton.Text = "G� til addresse"
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(10, 200)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(120, 23)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "RX-Innhold"
        '
        'rxContent
        '
        Me.rxContent.Location = New System.Drawing.Point(144, 200)
        Me.rxContent.Name = "rxContent"
        Me.rxContent.ReadOnly = True
        Me.rxContent.Size = New System.Drawing.Size(557, 22)
        Me.rxContent.TabIndex = 16
        '
        'UrlMonitorForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 15)
        Me.ClientSize = New System.Drawing.Size(712, 465)
        Me.Controls.Add(Me.rxContent)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.OpenButton)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Content)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Checked)
        Me.Controls.Add(Me.Changed)
        Me.Controls.Add(Me.PageSize)
        Me.Controls.Add(Me.URL)
        Me.Controls.Add(Me.URL_List)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "UrlMonitorForm"
        Me.ShowInTaskbar = False
        Me.Text = "NTB Web-Overv�ker"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Dim minimized As Boolean = False

    Dim tempFolder As String
    Dim smtpServer As String
    Dim fromAddress As String

    Dim monitorElements As ArrayList = New ArrayList()

    Private Sub CheckTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckTimer.Tick
        'Check the URLs
        CheckUrls()
    End Sub

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Load settings
        Dim cfg As New XmlDocument()
        Dim ndList As XmlNodeList
        Dim node As XmlNode

        Dim element As MonitorElement

        'Load settings
        Try
            tempFolder = ConfigurationManager.AppSettings("tempFolder")
            Directory.CreateDirectory(tempFolder)

            smtpServer = ConfigurationManager.AppSettings("smtpServer")
            fromAddress = ConfigurationManager.AppSettings("fromAddress")

            CheckTimer.Interval = ConfigurationManager.AppSettings("pollInterval") * 1000

            cfg.Load(ConfigurationManager.AppSettings("configFile"))
            ndList = cfg.SelectNodes("/urls/url")

            For Each node In ndList
                element = New MonitorElement()

                element.label = node.Attributes("label").Value
                element.url = node.Attributes("value").Value
                element.interval = node.Attributes("interval").Value

                'Optionals
                Try
                    element.email = node.Attributes("email").Value
                    element.popup = node.Attributes("popup").Value
                    element.openPage = node.Attributes("openPage").Value
                    element.rxCheck = node.Attributes("rxCheck").Value
                Catch
                End Try

                'Check temps
                Try
                    Dim reader As StreamReader = New StreamReader(tempFolder & "\" & element.label & ".html", Encoding.GetEncoding("iso-8859-1"))
                    element.content = reader.ReadToEnd
                    reader.Close()

                    element.pageSize = element.content.Length
                    element.lastCheck = Now
                    element.lastChanged = Now
                    element.checked = True

                    If Not String.IsNullOrEmpty(element.rxCheck) Then
                        Dim rx As Regex = New Regex(element.rxCheck, RegexOptions.Compiled Or RegexOptions.IgnoreCase)
                        Dim m As Match = rx.Match(element.content)
                        element.rxContent = m.Value
                    End If

                Catch
                End Try

                monitorElements.Add(element)
            Next

        Catch ex As Exception
            MessageBox.Show("Det oppsto en feil under lesing av konfigurasjonsfilene." & vbCrLf & vbCrLf & "Kontakt IT-avdelingen." & vbCrLf & vbCrLf & ex.Message, "Feil!")
        End Try

        'Populate selectbox
        For Each element In monitorElements
            URL_List.Items.Add(element.label)
        Next

        'Select first item
        URL_List.SelectedIndex = 0

        'Initial check
        CheckUrls()

        CheckTimer.Start()
    End Sub

    Private Sub URL_List_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles URL_List.SelectedIndexChanged
        Dim sel As Integer = URL_List.SelectedIndex
        Dim elm As MonitorElement = monitorElements(sel)

        Me.URL.Text = elm.url

        If elm.checked Then
            Me.PageSize.Text = elm.pageSize
            Me.Checked.Text = elm.lastCheck
            Me.Changed.Text = elm.lastChanged
            Me.Content.Text = elm.content
            Me.rxContent.Text = elm.rxContent
        Else
            Me.PageSize.Text = ""
            Me.Checked.Text = ""
            Me.Changed.Text = ""
            Me.Content.Text = ""
        End If

    End Sub

    'Loops the elements
    Private Sub CheckUrls()

        Dim element As MonitorElement

        Dim i As Integer
        For i = 0 To monitorElements.Count - 1

            element = monitorElements(i)

            If Now.Subtract(element.lastCheck).TotalSeconds >= element.interval Or Not element.checked Then
                'Do check
                CheckUrl(monitorElements(i))
            End If

        Next

        URL_List_SelectedIndexChanged(Me, New System.EventArgs())
    End Sub

    'Checks specific url
    Private Function CheckUrl(ByRef element As MonitorElement) As Boolean

        Dim oldrx As String = ""
        Dim old As String = ""

        'Create the HTTP request
        Dim request As HttpWebRequest = WebRequest.Create(element.url)
        request.Method = "GET"
        request.Timeout = 10000

        'Get the HTTP response with the resulting data
        Dim response As HttpWebResponse
        Try
            response = request.GetResponse()
        Catch webEx As WebException
            Return False
        End Try

        element.lastChanged = response.LastModified()

        Dim reader As StreamReader = New StreamReader(response.GetResponseStream(), Encoding.GetEncoding("iso-8859-1"))
        old = element.content
        oldrx = element.rxContent
        element.content = reader.ReadToEnd().Trim()
        element.pageSize = element.content.Length
        element.lastCheck = Now
        reader.Close()

        If Not String.IsNullOrEmpty(element.rxCheck) Then
            Dim rx As Regex = New Regex(element.rxCheck, RegexOptions.Compiled Or RegexOptions.IgnoreCase)
            Dim m As Match = rx.Match(element.content)
            element.rxContent = m.Value
        End If

        'Dump tempfile
        Try
            Dim writer As StreamWriter = New StreamWriter(tempFolder & "\" & element.label & ".html", False, Encoding.GetEncoding("iso-8859-1"))
            writer.Write(element.content)
            writer.Close()
        Catch
        End Try

        'Check for changes
        If element.checked Then

            If oldrx <> element.rxContent Or (old <> element.content And String.IsNullOrEmpty(element.rxCheck)) Then

                'Send email
                If element.email <> "" Then
                    Dim smtp As SmtpClient = New SmtpClient(smtpServer)
                    smtp.Send(fromAddress, element.email, element.label & " er endret!", element.label & " er blitt endret." & vbCrLf & "Sjekk dette!" & vbCrLf & vbCrLf & "505")
                End If

                'Open page
                If element.openPage Then
                    OpenURL(element.url)
                End If

                'Do popup
                If element.popup Then
                    MessageBox.Show(element.label & " er blitt endret. Sjekk dette! ", "Webside endret!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                End If

            End If

        End If

        'Control information
        element.checked = True

        Return True
    End Function

    Private Sub OpenURL(ByVal url As String)
        Dim st As ProcessStartInfo = New ProcessStartInfo()

        st.FileName = url
        st.UseShellExecute = True
        st.CreateNoWindow = False
        st.WindowStyle = ProcessWindowStyle.Normal

        System.Diagnostics.Process.Start(st)
    End Sub

    Private Sub TrayIcon_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TrayIcon.DoubleClick
        Me.Show()
        Me.Activate()
        Me.WindowState = FormWindowState.Normal
        minimized = False
    End Sub

    Private Sub TrayMenuOpen_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles TrayMenuOpen.Click
        Me.Show()
        Me.Activate()
        Me.WindowState = FormWindowState.Normal
        minimized = False
    End Sub

    Private Sub TrayMenuExit_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles TrayMenuExit.Click
        minimized = True
        Me.Close()
    End Sub

    Private Sub UrlMonitorForm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        'Prevent close if made from active window
        If Not minimized Then
            minimized = True
            Me.WindowState = FormWindowState.Minimized
            Me.Hide()
            e.Cancel = True
        End If
    End Sub

    Private Sub OpenButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenButton.Click
        OpenURL(URL.Text)
    End Sub
End Class
