Public Class MonitorElement

    'Page to monitor
    Public label As String
    Public url As String
    Public interval As Integer

    'Alerts options 
    Public email As String = ""
    Public popup As Boolean = False
    Public openPage As Boolean = False

    'Recieved content
    Public lastCheck As DateTime
    Public lastChanged As DateTime
    Public pageSize As Integer
    Public content As String

    Public rxCheck As String
    Public rxContent As String

    'Control variable
    Public checked As Boolean = False

End Class
